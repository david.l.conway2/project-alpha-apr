from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import TaskForm
from .models import Task

# Create your views here.


def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = TaskForm()

    context = {"form": form}

    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    user = request.user
    tasks = Task.objects.filter(assignee=user)
    context = {"tasks": tasks}
    return render(request, "tasks/show_my_tasks.html", context)
